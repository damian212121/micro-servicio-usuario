FROM openjdk:13
VOLUME /tmp
ADD ./target/micro-servicio-usuario-0.0.1-SNAPSHOT.jar servicio-usuarios.jar
ENTRYPOINT ["java","-jar","/servicio-usuarios.jar"]
