INSERT INTO usuarios(username,password,enabled, nombre , apellido,email) VALUES ('admin','$2a$10$6Xz5SAilg8C5Oy2R/K8rIuKXxYuyKtJynqkiEO1Ucuzm/2xvC5cTq',true ,'Nicolas','Lescano','nlescano.m.m.m@gmail.com');
INSERT INTO usuarios(username,password,enabled, nombre , apellido,email) VALUES ('damian2121','$2a$10$H8pX9Mf/rYlzP6ws8dT39uKTSh5Et4FHtYw/ok4VnvCu7a.lAcQ92',true ,'damian','Rieu','nRieu.m.m.m@gmail.com');

INSERT INTO roles (nombre) VALUES ('ROLE_USER')
INSERT INTO roles (nombre) VALUES ('ROLE_ADMIN')

INSERT INTO usuarios_roles (usuario_id, role_id) VALUES (1,1)
INSERT INTO usuarios_roles (usuario_id, role_id) VALUES (2,2)
INSERT INTO usuarios_roles (usuario_id, role_id) VALUES (2,1)